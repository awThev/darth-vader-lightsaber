import kaboom from "https://unpkg.com/kaboom/dist/kaboom.mjs";

kaboom({
  scale: 6,
  background: [213, 216, 220],
});

loadSound("saberOn", "/sounds/saberOn.mp3");
loadSound("saberOff", "/sounds/saberOff.mp3");
loadSound("saberIdle", "/sounds/saberIdle.mp3");
loadSprite("darth_vader_off", "sprites/darth_vader_off.png");
loadSprite("darth_vader", "sprites/darth_vader.png", {
  sliceX: 16,
  anims: {
    idle_off: {
      from: 0,
      to: 0,
      speed: 10,
      loop: true,
    },
    turn_on: {
      from: 0,
      to: 14,
      speed: 40,
      loop: false,
    },
    idle_on: {
      from: 14,
      to: 14,
      speed: 10,
      loop: true,
    },
    turn_off: {
      from: 14,
      to: 0,
      speed: 20,
      loop: false,
    },
  },
});

const saberIdle = play("saberIdle", { loop: true });

let on = false;

volume(0);

scene("wait", () => {
  volume(1);
  saberIdle.pause();
  add([sprite("darth_vader_off"), pos(center()), origin("center")]);

  add([
    rect(width(), 150),
    area(),
    outline(1, rgb(0, 0, 0)),
    pos(0, height() / 2 + 16),
    solid(),
    color(86, 101, 115),
  ]);

  add([
    text("Press f to toggle fullscreen", { size: 10 }),
    pos(width() / 2, height() / 2 + 30),
    origin("center"),
  ]);
  add([
    text("Press space to release", { size: 10 }),
    pos(width() / 2, height() / 2 + 45),
    origin("center"),
  ]);

  onKeyPress("space", () => {
    go("release");
    on = true;
  });
  onKeyPress("f", () => {
    fullscreen(!isFullscreen());
  });
});

scene("release", () => {
  onKeyPress("f", () => {
    fullscreen(!isFullscreen());
  });
  add([
    rect(width(), 150),
    area(),
    outline(1, rgb(0, 0, 0)),
    pos(0, height() / 2 + 16),
    solid(),
    color(86, 101, 115),
  ]);

  const darthVarder = add([
    sprite("darth_vader"),
    pos(center()),
    origin("center"),
  ]);

  darthVarder.play("turn_on");

  play("saberOn", {
    loop: false,
  });

  darthVarder.onAnimEnd("idle_on");

  wait(1, () => {
    saberIdle.play();
  });

  // TODO : check actual frame et saberOn à partir de cette frame

  onKeyPress("space", () => {
    saberIdle.pause();
    if (on == true) {
      saberIdle.pause();
      on = false;
      darthVarder.play("turn_off");
      play("saberOff", {
        loop: false,
      });
      darthVarder.onAnimEnd("idle_off");
    } else {
      on = true;
      darthVarder.play("turn_on");
      play("saberOn", {
        loop: false,
      });
      wait(1, () => {
        saberIdle.play();
      });
      darthVarder.onAnimEnd("idle_on");
    }
  });
});

go("wait");
